import React from "react";
import { bool, func } from "prop-types";
import { StyledBurger } from "./Burger.styled";

const Burger = ({
  openMenu,
  setOpenMenu,
  openCartList,
  setOpenCartList,
}) => {
  const isExpanded = openMenu ? true : false;

  const updateMenu = (openMenu) => {
    setOpenMenu(!openMenu);
    setOpenCartList(false);
  };

  return (
    <StyledBurger
      aria-label="Toggle menu"
      aria-expanded={isExpanded}
      openMenu={openMenu}
      onClick={() => updateMenu(openMenu)}
    >
      <span />
      <span />
      <span />
    </StyledBurger>
  );
};

Burger.propTypes = {
  openMenu: bool.isRequired,
  setOpenMenu: func.isRequired,
};

export default Burger;
