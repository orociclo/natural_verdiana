import React, { useEffect } from "react";
import { StyledHeader } from "./Header.styled";
import { Burger } from "../../components";

import imageLeaf from "../../images/leaf_left.png";
import imageCartList from "../../images/cart.svg";
import imageClose from "../../images/close.png";

const Header = ({
  openMenu,
  setOpenMenu,
  openCartList,
  setOpenCartList,
  cartList,
  setRequestStatus,
}) => {
  const updateOpenCartList = (openCartList) => {
    setOpenCartList(!openCartList);
    setOpenMenu(false);
    if (openCartList) {
      setTimeout(setRequestStatus(0), 500);
    }
  };

  // useEffect(() => {
  //   console.log("CartList changed!!");
  // }, [cartList]);

  return (
    <StyledHeader id="Header">
      <div id="headerTop"></div>
      <div id="headerMiddle">
        <div id="divContainerLeft">
          <Burger
            openMenu={openMenu}
            setOpenMenu={setOpenMenu}
            setOpenCartList={setOpenCartList}
          />
        </div>
        <div id="divContainerCenter">
          <img id="ImageLeafLeft" src={imageLeaf} alt="LeafLeft" />
          <h3 id="divTextCenter">Natural Verdiana</h3>
          <img id="ImageLeafRight" src={imageLeaf} alt="LeafRight" />
        </div>
        <div
          id="divContainerRight"
          onClick={() => updateOpenCartList(openCartList)}
        >
          <button id="buttonCartList">
            {!openCartList && (
              <div id="divCartListButtonInner">
                <img
                  id="imgCartList"
                  className="biggerCart"
                  src={imageCartList}
                  alt="Cart"
                />
                <div id="divCartListNumberContainer">
                  <div id="divCartListNumber">{cartList.length}</div>
                </div>
              </div>
            )}
            {openCartList && (
              <div id="divCloseCartListContainer">
                <img
                  id="imgClose"
                  src={imageClose}
                  alt="Close"
                  onClick={() => setOpenCartList(false)}
                />
              </div>
            )}
          </button>
        </div>
      </div>
      <div id="headerBottom"></div>
    </StyledHeader>
  );
};

export default Header;
