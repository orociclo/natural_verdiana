import styled from "styled-components";

export const StyledHeader = styled.div`
  position: fixed;
  z-index: 12;
  /* overflow-y: scroll;
  height: 100vh; */
  display: flex;
  flex-direction: column;
  width: 100%;
  background: ${({ theme }) => theme.primaryDark};
  color: ${({ theme }) => theme.primaryLight};
  height: 45px;
  border-bottom-left-radius: 50px;
  border-bottom-right-radius: 50px;
  .normalCart {
    height: 100%;
  }
  .biggerCart {
    height: 100%;
  }
  #headerMiddle {
    display: flex;
    flex-direction: row;
    align-items: center;
    /* padding: 0.5rem 0.5rem; */
    height: 2.5rem !important;
    #divContainerLeft {
      display: flex;
      flex: 0 0 5rem;
      button {
        padding: 1px 6px;
      }
    }
    #divContainerCenter {
      display: flex;
      flex-direction: row;
      flex: 1;
      justify-content: center;
      align-items: center;
      width: 30vw;
      /* padding-top: 5px; */
      #ImageLeafLeft {
        width: 3.125rem !important;
        height: auto;
      }
      #divTextCenter {
        text-align: center;
        padding-top: 3px;
        width: min-content;
      }
      #ImageLeafRight {
        width: 3.125rem !important;
        height: auto;
        transform: rotate(180deg);
      }
    }
    #divContainerLeft {
      flex: 0 0 5rem;
      display: flex;
      justify-content: flex-start;
      padding-left: 2rem;
      #buttonCartList {
        background-color: inherit;
        border: none;
        cursor: pointer;
        padding: 0;
        margin: 0;

        #divCartListButtonInner {
          position: relative;
          width: 100%;
          height: 100%;
          #imgCartList {
            padding: 0;
          }

          #divCartListNumberContainer {
            position: absolute;
            top: 0;
            left: 50%;
            transform: translate(-40%, -40%);
            #divCartListNumber {
              animation: fade-in 0.4s linear forwards;
              font-size: 0.9rem;
              line-height: 1;
              color: #fff;
            }
          }
        }
      }
    }
    #divContainerRight {
      flex: 0 0 5rem;
      display: flex;
      justify-content: flex-end;
      padding-right: 2rem;
      padding-top: 5px;
      /* padding: 0 0.25rem; */
      #buttonCartList {
        background-color: inherit;
        border: none;
        cursor: pointer;
        padding: 0;
        margin: 0;
        #divCartListButtonInner {
          position: relative;
          width: 100%;
          height: 100%;
          #imgCartList {
            padding: 0;
          }
          #divCartListNumberContainer {
            position: absolute;
            top: 0;
            left: 50%;
            transform: translate(-40%, -40%);
            #divCartListNumber {
              animation: fade-in 0.4s linear forwards;
              font-size: 1rem;
              line-height: 1;
              color: #fff;
              padding-left: 5px;
              padding-top: 3px;
            }
          }
        }
        #divCloseCartListContainer {
          display: flex;
          justify-content: end;
          width: 100%;
          cursor: pointer;
          color: ${({ theme }) => theme.primaryLight};
          img {
            width: 25px;
          }
        }
      }
    }
  }
`;
