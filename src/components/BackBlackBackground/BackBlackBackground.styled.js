import styled from 'styled-components';

export const StyledBackBlackBackground = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  background-color: black;
  opacity: 0.75;
  z-index: 11;
  overflow-y: hidden;
`;
