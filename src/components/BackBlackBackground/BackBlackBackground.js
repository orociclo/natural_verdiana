import React from "react";
import { StyledBackBlackBackground } from "./BackBlackBackground.styled";

const BackBlackBackground = ({ setOpenCartList }) => {
  const handleClick = (event) => {
    event.stopPropagation();
    setOpenCartList(false);
  };

  return (
    <StyledBackBlackBackground
      id="divBackBlackBackground"
      onClick={handleClick}
    />
  );
};

export default BackBlackBackground;
