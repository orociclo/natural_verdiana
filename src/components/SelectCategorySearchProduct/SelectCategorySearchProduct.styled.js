import styled from "styled-components";
import { theme } from "../../theme";

export const StyledSelectCategorySearchProduct = styled.div`
  text-align: center;
  width: 90%;
  padding-top: 20px;
  @media (min-width: ${({ theme }) => theme.mobile}) {
    padding-top: 10px;
  }

  #divCategorySelectionMain {
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    width: 100%;
    height: 4rem;
    @media (max-width: ${({ theme }) => theme.mobile}) {
      flex-direction: column;
      margin-top: 35px;
    }
    #divSearchProduct {
      display: flex;
      width: 30%;
      /* background-color: ${({ theme }) => theme.primaryDark}; */
      border-radius: 2rem;
      justify-content: center;
      cursor: pointer;
      @media (max-width: ${({ theme }) => theme.mobile}) {
        width: 101%;
        margin-bottom: 10px;
      }
      #selectReact {
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: ${({ theme }) => theme.primaryDark};
        width: 100%;
        border-radius: 50px;
        height: 100%;
      }
    }
    #divSearchProduct:focus {
      color: red;
    }
    #divChangeProductCategory {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 30%;
      /* box-shadow: 2px 5px #888; */
      font-weight: bold;
      font-size: 1rem;
      background: ${({ theme }) => theme.primaryDark};
      color: ${({ theme }) => theme.primaryLight};
      border-radius: 4rem;
      cursor: pointer;
      padding: 10px;
      label {
        cursor: pointer;
      }
      /* margin: auto; */
      @media (max-width: ${({ theme }) => theme.mobile}) {
        width: 100%;
      }
    }
    #divCategorySelected {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 30%;
      font-weight: bold;
      font-size: 1rem;
      background: #fff;
      color: ${({ theme }) => theme.primaryDark};
      border-top-left-radius: 2rem;
      border-top-right-radius: 2rem;
      border-style: solid;
      border-width: 3px;
      border-color: ${({ theme }) => theme.primaryDark};
      padding: 5x;
      @media (max-width: ${({ theme }) => theme.mobile}) {
        width: 100%;
        margin-top: 7px;
      }
      label {
        padding-bottom: 5px;
        padding-top: 7px;
        padding-left: 10px;
        padding-right: 10px;
      }
    }
  }

  #divListContainer {
    position: absolute;
    background-color: transparent;
    z-index: 10;
    width: 90%;
    display: flex;
    justify-content: center;
  }

  .divList {
    overflow-y: auto;
    overflow-x: hidden;
    font-weight: bold;
    line-height: 1rem;
    height: 75vh;
    width: 30%;
    background-color: ${({ theme }) => theme.primaryLight};
    @media (max-width: ${({ theme }) => theme.mobile}) {
      width: 100%;
      height: 65vh;
      /* margin-top: 1rem; */
    }
    /* ::-webkit-scrollbar {
      width: 20px;
    } */

    /* Track */
    /* ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px grey;
      border-radius: 10px;
    } */

    /* Handle */
    /* ::-webkit-scrollbar-thumb {
      background: ${({ theme }) => theme.primaryDark};
      border-radius: 10px;
    } */

    /* Handle on hover */
    /* ::-webkit-scrollbar-thumb:hover {
      background: ${({ theme }) => theme.primaryDarkDark};
    }
    @media (max-width: ${({ theme }) => theme.mobile}) {
      height: 85vh;
    } */
  }

  .ListItem {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    list-style: none;
    margin-top: 0.8em;
    /* box-shadow: 2px 5px #888; */
    font-size: 1rem;
    background: #92c03c;
    color: #effffa;
    border-radius: 4rem;
    line-height: 2rem;
    padding-left: 2rem;
    padding-right: 2rem;
    width: 99.1%;
    margin-bottom: 5px;
    cursor: pointer;
    &:hover {
      color: ${({ theme }) => theme.primaryDark};
      background-color: ${({ theme }) => theme.primaryLight};
      cursor: pointer;
    }
    .imgageListItem {
      height: auto;
      width: 6rem;
      padding-top: 5px;
    }
  }
`;

export const customStyles = {
  color: "red",
  menu: (provided, state) => ({
    ...provided,
    color: "white",
    fontSize: "1.5rem",
    backgroundColor: theme.primaryLight,
    boxShadow: "none",
    marginTop: 5,
    border: "none",
  }),
  menuList: (provided, state) => ({
    ...provided,
    minHeight: "70vh",
  }),
  input: (provided, state) => ({
    ...provided,
    color: state.isSelected ? theme.primaryLightPale : theme.primaryLight,
    fontWeight: "bold",
    border: "none",
  }),
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? theme.primaryLightPale : theme.primaryLight,
    backgroundColor: state.isFocused
      ? theme.primaryDarkDark
      : theme.primaryDark,
    padding: 5,
    marginBottom: 10,
    borderRadius: 50,
    fontSize: "1rem",
    fontWeight: "bold",
    border: "none",
  }),
  control: (provided, state) => ({
    ...provided,
    display: "flex",
    backgroundColor: theme.primaryDark,
    color: theme.primaryLight,
    fontWeight: "bold",
    borderRadius: 50,
    border: "none",
    with: "100%",
    boxShadow: "none",
  }),
  region: (provided, state) => ({
    ...provided,
    border: "none",
    with: "100%",
  }),
  dropdownIndicator: (provided, state) => ({
    ...provided,
    display: "none",
  }),
  indicatorSeparator: (provided, state) => ({
    ...provided,
    display: "none",
  }),
};
