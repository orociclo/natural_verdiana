import React, { useState, useEffect } from "react";
import Select from "react-select";
import { getTranslation } from "../../tools/tools";

import { StyledSelectCategorySearchProduct, customStyles } from "./SelectCategorySearchProduct.styled";

const SelectCategorySearchProduct = ({
  handleOnChangeCategory,
  categories,
  setCategoryData,
  groupCategories,
  lang,
}) => {
  console.log("On select category ................: ", categories)
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState();
  const [storedSelectedOption, setStoredSelectedOption] = useState();

  useEffect(() => {
    // When the language is changed, update the selected category text
    if (selectedOption) {
      const getTranslationOption = getTranslation(
        selectedOption,
        lang
      );
      setStoredSelectedOption(getTranslationOption);
    }
  }, [lang]);

  const content = {
    es: {
      typeProduct: "TECLEAR PRODUCTO A BUSCAR",
      selectCategory: "SELECCIONAR CATEGORIA",
    },
    en: {
      typeProduct: "TYPE PRODUCT TO SEARCH",
      selectCategory: "SELECT CATEGORY",
    },
    fr: {
      typeProduct: "TAPEZ PRODUIT À RECHERCHER",
      selectCategory: "SÉLECTIONNEZ CATÉGORIE",
    },
    de: {
      typeProduct: "SUCHEN SIE NACH PRODUKT",
      selectCategory: "WÄHLEN SIE KATEGORIE AUS",
    },
  };

  const changeProductCategory = () => {
    setIsOpen(!isOpen);
  };

  const onOptionClicked = (categoryIndex) => {
    console.log("categoryIndes ", categoryIndex, categories[categoryIndex])
    setCategoryData([]);
    const translatedCategory = getTranslation(
      categories[categoryIndex].name,
      categories[categoryIndex].name_en,
      categories[categoryIndex].name_fr,
      categories[categoryIndex].name_de,
      lang
    );
    setSelectedOption(translatedCategory);
    setIsOpen(false);
    // Selecting Categories, call the categories filter
    handleOnChangeCategory(categoryIndex);
    const getTranslationOption = getTranslation(
      categories[categoryIndex].name,
      categories[categoryIndex].name_en,
      categories[categoryIndex].name_fr,
      categories[categoryIndex].name_de,
      lang
    );
    if (getTranslationOption) {
      setStoredSelectedOption(getTranslationOption);
    }
  };

  const handleSelectInputChange = (inputValue, actionMeta) => {
    console.log(`action: ${actionMeta.action}`);
    setCategoryData([]);
    setSelectedOption(categories[inputValue.categoryIndex].name);
    setIsOpen(false);
    // Selecting Categories, call the categories filter
    handleOnChangeCategory(inputValue.categoryIndex, inputValue.productIndex);
    const getTranslationOption = getTranslation(
      categories[inputValue.categoryIndex].name,
      lang
    );
    console.log("getTranslationOption ", getTranslationOption);
    setStoredSelectedOption(getTranslationOption);
  };

  const handleSelectOnFocus = () => {
    console.log("On open menu");
    setIsOpen(false);
  };

  return (
    <StyledSelectCategorySearchProduct id="divSelectionProductAndCategoryContainer">
      <div id="divCategorySelectionMain">
        <div id="divSearchProduct">
          <Select
            id="selectReact"
            autofocus={true}
            onChange={handleSelectInputChange}
            options={groupCategories}
            onFocus={handleSelectOnFocus}
            noOptionsMessage={({ inputValue }) =>
              !inputValue ? "noOptionMessage" : "Producto no encontrado"
            }
            placeholder={
              <div style={{ color: "#fff", fontWeight: "bold" }}>
                {content[lang].typeProduct}
              </div>
            }
            styles={customStyles}
          />
        </div>
        <div id="divChangeProductCategory" onClick={changeProductCategory}>
          <label>{content[lang].selectCategory}</label>
        </div>
        {console.log("selectedOption ", selectedOption)}
        {selectedOption && <div id="divCategorySelected">
          <label>
            {lang === "es" && selectedOption}
            {lang !== "es" && storedSelectedOption && (
              <div>{storedSelectedOption}</div>
            )}
          </label>
        </div>}
      </div>
      {isOpen && (
        <div id="divListContainer">
          <div className="divList">
            {categories.map((category, categoryIndex) => {
              const storedCategory = getTranslation(category.name, category.name_en, category.name_fr, category.name_de,lang);
              return (
                <li
                  className="ListItem"
                  onClick={() => onOptionClicked(categoryIndex)}
                  key={Math.random()}
                >
                  {lang === "es" && category.name}
                  {lang !== "es" && storedCategory && (
                    <div>{storedCategory}</div>
                  )}
                </li>
              );
            })}
          </div>
        </div>
      )}
    </StyledSelectCategorySearchProduct>
  );
};

export default SelectCategorySearchProduct;
