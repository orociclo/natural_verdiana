import React from "react";

import { StyledChangeQuantityOnCartList } from "./ChangeQuantityOnCartList.styled";

const ChangeQuantityOnCartList = ({
  handleChangeItemQuantity,
  cartItemIndex,
  cartList,
}) => {
  const handleLess = () => {
    if (cartList[cartItemIndex].quantity > 0) {
      handleChangeItemQuantity(cartItemIndex, -1);
    }
  };

  const handleMore = () => {
    if (cartList[cartItemIndex].quantity < 50) {
      handleChangeItemQuantity(cartItemIndex, 1);
    }
  };

  return (
    <StyledChangeQuantityOnCartList>
      <div id="divChangeQuantityMain">
        <div id="divLess" onClick={handleLess}>
          -
        </div>
        <div id="divValue">
          {cartList[cartItemIndex] ? cartList[cartItemIndex].quantity : 0}
        </div>
        <div id="divMore" onClick={handleMore}>
          +
        </div>
      </div>
    </StyledChangeQuantityOnCartList>
  );
};

export default ChangeQuantityOnCartList;
