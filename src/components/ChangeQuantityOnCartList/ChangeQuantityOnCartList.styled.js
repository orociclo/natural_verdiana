import styled from "styled-components";

export const StyledChangeQuantityOnCartList = styled.div`
  #divChangeQuantityMain {
    display: flex;
    justify-content: center;
    flex-direction: row;

    #divLess {
      padding-left: 10px;
      padding-right: 10px;
      font-size: 2rem;
      border-top-left-radius: 50%;
      border-bottom-left-radius: 50%;
      background: ${({ theme }) => theme.primaryDark};
      color: ${({ theme }) => theme.primaryLight};
      cursor: pointer;
      /* box-shadow: 2px 5px rgb(8 8 8 / 15%); */
    }
    #divMore {
      padding-left: 10px;
      padding-right: 10px;
      font-size: 2rem;
      border-top-right-radius: 50%;
      border-bottom-right-radius: 50%;
      background: ${({ theme }) => theme.primaryDark};
      color: ${({ theme }) => theme.primaryLight};
      cursor: pointer;
      /* box-shadow: 2px 5px rgb(8 8 8 / 15%); */
    }
    #divValue {
      padding-left: 10px;
      padding-right: 10px;
      font-size: 2rem;
      background: ${({ theme }) => theme.primaryLight};
      color: ${({ theme }) => theme.primaryDark};
      width: 4rem;
    }
  }
`;
