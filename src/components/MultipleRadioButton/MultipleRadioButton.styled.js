import styled from "styled-components";

import RadioButtonChecked from "../../images/radio_button_checked.png";
import RadioButtonUnchecked from "../../images/radio_button_unchecked.png";

export const StyledMultipleRadioButton = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 50%;
  @media (max-width: ${({ theme }) => theme.mobile}) {
    width: 100%;
  }
  #divMultipleRadioButtonContainer {
    display: flex;
    flex-direction: column;
    width: 100%;
    border-radius: 4rem;
    border-style: solid;
    border-width: 3px;
    border-color: ${({ theme }) => theme.primaryDark};
    padding: 5px;
    margin-left: 5px;
    margin-right: 5px;
    color: #000;
    #labelTitleMultipleRadioButton {
      font-size: 0.85rem;
    }
    form {
      display: flex;
      width: 100%;
      justify-content: space-evenly;
      div {
        
        input[type="radio"] {
          display: none;
        }

        input[type="radio"] + label {
          display: flex;
          flex-direction: column;
          align-items: center;
          color: ${({ theme }) => theme.primaryDark};
          font-family: Arial, sans-serif;
          font-size: 0.85rem;
          color: #000;
        }

        input[type="radio"] + label span {
          display: inline-block;
          background-repeat: no-repeat;
          width: 19px;
          height: 19px;
          vertical-align: middle;
          background: url(${RadioButtonUnchecked});
          background-size: contain;
        }
        cursor: pointer;
        input[type="radio"]:checked + label span {
          background: url(${RadioButtonChecked});
          background-size: contain;
        }
      }
    }
  }
`;
