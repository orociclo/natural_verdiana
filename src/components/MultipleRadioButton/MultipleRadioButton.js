import React, { useState } from "react";
import { StyledMultipleRadioButton } from "./MultipleRadioButton.styled";

const MultipleRadioButton = ({ title, optionArray, day, setDay, hour, setHour, type }) => {

  const handleOnChange = (e) => {
    console.log("radio ", e.target.value);
    if (e.target.value.includes("h.")) {
      setHour(e.target.value);
      console.log("is an hour")
    } else {
      setDay(e.target.value);
      console.log("is a day")
    }
  };

  return (
    <StyledMultipleRadioButton id="divMultipleRadioButtonComponent">
      <div id="divMultipleRadioButtonContainer">
        <label id="labelTitleMultipleRadioButton">{title}</label>
        <form id={type}>
          {optionArray.map((option, index) => {
            return (
              <div className="divOptionItem" key={index}>
                <input
                  type="radio"
                  id={`${title}-${index}`}
                  name={title}
                  value={option}
                  onChange={handleOnChange}
                  checked={option.includes("h.") ? option === hour : option === day}
                />
                <label htmlFor={`${title}-${index}`}>
                  <span></span>
                  <label>{option}</label>
                </label>
              </div>
            );
          })}
        </form>
      </div>
    </StyledMultipleRadioButton>
  );
};

export default MultipleRadioButton;
