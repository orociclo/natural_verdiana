import styled from 'styled-components';

export const StyledMenu = styled.nav`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: ${({ theme }) => theme.primaryLight};
  transform: ${({ openMenu }) => openMenu ? 'translateX(0)' : 'translateX(-100%)'};
  height: 100vh;
  text-align: left;
  padding: 2rem;
  position: absolute;
  top: 0;
  left: 0;
  transition: transform 0.3s ease-in-out;
  z-index: 11;
  transition: width 2s;
  width: 500px;
  @media (max-width: ${({ theme }) => theme.mobile}) {
    width: 100%;
  }

  a {
    font-size: 2rem;
    text-transform: uppercase;
    padding: 2rem 0;
    font-weight: bold;
    letter-spacing: 0.5rem;
    color: ${({ theme }) => theme.primaryDark};
    text-decoration: none;
    transition: color 0.3s linear;

    @media (max-width: ${({ theme }) => theme.mobile}) {
      font-size: 1.5rem;
      text-align: center;
    }

    &:hover {
      color: ${({ theme }) => theme.primaryHover};
    }
  }

  p {
    font-size: ${({ theme }) => theme.bigFontSize};
    text-transform: uppercase;
    font-weight: bold;
    letter-spacing: 0.1rem;
    color: ${({ theme }) => theme.primaryDark};
    text-decoration: none;
    /* transition: color 0.3s linear; */
    transition: width 2s;
    cursor:pointer;
    border: none;
    cursor: pointer;
    border-radius: 50px;
    background-color:  ${({ theme }) => theme.primaryDark};
    color: #fff;
    text-align: center;
    font-size: 2rem;
    padding: 1rem;
    margin: 0px;
    @media (max-width: ${({ theme }) => theme.mobile}) {
      font-size: 1.5rem;
    }

    &:hover {
      color: ${({ theme }) => theme.primaryHover};
      /* background-color:  ${({ theme }) => theme.primaryLight}; */
    }
  }
`;
