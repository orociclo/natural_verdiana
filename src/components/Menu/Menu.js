import React from "react";
import FlagRadioButton from "../FlagRadioButton";
import { bool } from "prop-types";
import { StyledMenu } from "./Menu.styled";

const Menu = ({ openMenu, setOpenMenu, setPageOpen, lang, setLang, groupCategories }) => {
  const isHidden = openMenu ? true : false;

  const content = {
    es: {
      page01: "La esencia",
      page02: "Pedidos",
      page03: "Contacto",
      selectCountry: "Selectión de Idioma",
    },
    en: {
      page01: "The essence",
      page02: "Orders",
      page03: "Contact",
      selectCountry: "Language Selection",
    },
    fr: {
      page01: "L'essence",
      page02: "Commandes ",
      page03: "Contact",
      selectCountry: "Sélection de la langue",
    },
    de: {
      page01: "Die Essenz",
      page02: "Aufforderungen",
      page03: "Kontakt",
      selectCountry: "Sprachauswahl",
    },
  };

  return (
    <StyledMenu id="divMenu" openMenu={openMenu} aria-hidden={!isHidden}>
      {openMenu && (
        <div id="divMenuContent">
          <p
            onClick={() => {
              setPageOpen(0);
              setOpenMenu(false);
            }}
          >
            {content[lang].page01}
          </p>
          <p
            onClick={() => {
              setPageOpen(1);
              setOpenMenu(false);
            }}
          >
            {content[lang].page02}
          </p>
          <p
            onClick={() => {
              setPageOpen(2);
              setOpenMenu(false);
            }}
          >
            {content[lang].page03}
          </p>
          <FlagRadioButton
            lang={lang}
            setLang={setLang}
            style={{ width: "50%" }}
            groupCategories={groupCategories}
          />
        </div>
      )}
    </StyledMenu>
  );
};

Menu.propTypes = {
  openMenu: bool.isRequired,
};

export default Menu;
