import React, { useState, useEffect } from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";

import { StyledProductSwiper } from "./ProductSwiper.styled";

import right from "../../images/right.png";
import left from "../../images/left.png";

const ProductSwiper = ({
  htmlProductList,
  indexCategorySelected,
  indexProductSelected = null,
}) => {
  const [activeIndex, setActiveIndex] = useState(indexProductSelected);

  // When change category, reset to first page on swiper
  useEffect(() => {
    setActiveIndex(indexProductSelected);
  }, [indexCategorySelected, indexProductSelected]);

  const responsive = {
    0: { items: 1 },
    576: { items: 3 },
    1024: { items: 4 },
  };

  const slideNext = (e) => {
    console.log("e ", e.target.parent);
    if (activeIndex < htmlProductList.length - 1) {
      setActiveIndex(activeIndex + 1);
    }
  };
  const slidePrev = () => {
    if (activeIndex > 0) {
      setActiveIndex(activeIndex - 1);
    }
  };

  const syncActiveIndex = ({ item }) => {
    console.log("item ", item)
    setActiveIndex(item)
  };

  return (
    <StyledProductSwiper id="divProductSwiper">
      {htmlProductList.length > 0 && (
        <>
          <div id="divImgPrev">
            {activeIndex > 0 && (
              <img id="imgPrev" src={left} alt="left" onClick={slidePrev} />
            )}
          </div>
          <AliceCarousel
            activeIndex={activeIndex}
            mouseTracking
            items={htmlProductList}
            responsive={responsive}
            disableDotsControls
            disableButtonsControls
            onSlideChanged={syncActiveIndex}
            animationType="slide"
            animationDuration={200}
          />
          <div id="divMovileImg">
            {activeIndex > 0 && (
              <div id="divImgPrev2">
                <img id="imgPrev" src={left} alt="left" onClick={slidePrev} />
              </div>
            )}
            <div id="divImgNext">
              {activeIndex < htmlProductList.length - 1 && (
                <img id="imgNext" src={right} alt="right" onClick={slideNext} />
              )}
            </div>
          </div>
        </>
      )}
    </StyledProductSwiper>
  );
};

export default ProductSwiper;
