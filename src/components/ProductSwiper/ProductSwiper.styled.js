import styled from "styled-components";

export const StyledProductSwiper = styled.div`
  flex-direction: row;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  width: 90%;
  height: 80vh;
  /* margin: auto; */
  margin-top: 10px;
  @media (max-width: ${({ theme }) => theme.mobile}) {
    margin-top: 50px;
    flex-direction: column;
  }
  #divImgPrev {
    width: 3rem;
    @media (max-width: ${({ theme }) => theme.mobile}) {
      display: none;
    }
    #imgPrev {
      width: 3rem;
      cursor: pointer;
      :focus {
        outline: none;
      }
    }
  }
  #divMovileImg {
    @media (max-width: ${({ theme }) => theme.mobile}) {
      display: flex;
      flex-direction: row;
      width: 100%;
      justify-content: space-evenly;
    }
  }
  #divImgPrev2 {
    display: none;
    width: 3rem;
    @media (max-width: ${({ theme }) => theme.mobile}) {
      display: block;
    }
    #imgPrev {
      width: 3rem;
      cursor: pointer;
      :focus {
        outline: none;
      }
    }
  }
  #divImgNext {
    width: 3rem;
    #imgNext {
      width: 3rem;
      cursor: pointer;
      :focus {
        outline: none;
      }
    }
  }
  img {
    width: 100%;
  }
  .alice-carousel {
    width: 80%;
    margin: 0px;
  }
  .alice-carousel__dots {
    margin-top: 5px;
    @media (min-width: ${({ theme }) => theme.mobile}) {
      margin-top: 5px;
    }
  }
  .alice-carousel__wrapper {
    // Altura del carrusel
    /* padding-bottom: 5rem;
    @media (min-width: ${({ theme }) => theme.mobile}) {
      padding-bottom: 10rem;
    } */
    padding-bottom: 10rem;
    @media only screen and (max-height: 480px) {
      padding-bottom: 3rem;
    }
    @media only screen and (max-width: 760px) {
      padding-bottom: 3rem;
    }
  }
  .alice-carousel__dots-item:not(.__custom).__active {
    background-color: ${({ theme }) => theme.primaryDark};
    height: 20px;
    width: 20px;
  }
  .alice-carousel__dots-item:not(.__custom) {
    background-color: ${({ theme }) => theme.primaryLightPale};
    height: 20px;
    width: 20px;
  }
  .alice-carousel__dots-item:not(.__custom):hover {
    background-color: ${({ theme }) => theme.primaryDarkDark};
    height: 20px;
    width: 20px;
  }
`;
