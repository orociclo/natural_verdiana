import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faYoutube,
  faFacebook,
  faTwitter,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import { StyledSocialFollow } from './SocialFollow.styled';

const SocialFollow = () => {

  return (
    <StyledSocialFollow>
      <div className="social-container">
        <h2>Redes Sociales</h2>
        <a href="https://www.youtube.com/channel/UCtE86vzPenTmoTuF6pqN7aw"
          className="youtube social">
          <FontAwesomeIcon icon={faYoutube} size="2x" />
        </a>
        <a href="https://www.facebook.com/Bocado.de.salud/"
          className="facebook social">
          <FontAwesomeIcon icon={faFacebook} size="2x" />
        </a>
        <a href="https://www.twitter.com/" className="twitter social">
          <FontAwesomeIcon icon={faTwitter} size="2x" />
        </a>
        <a href="https://www.instagram.com/"
          className="instagram social">
          <FontAwesomeIcon icon={faInstagram} size="2x" />
        </a>
      </div>
    </StyledSocialFollow>
  )
}

export default SocialFollow;