import styled from 'styled-components';

export const StyledSocialFollow=styled.div`

.social-container {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: ${({ theme }) => theme.primaryDark};
  color: ${({ theme }) => theme.primaryLight};
  text-rendering: optimizeLegibility;
  font-family: 'Roboto Regular', sans-serif;
  padding: 20px;
  border-radius: 50px;
  margin-bottom: 30px;
}

a.social {
  margin: 0 1rem;
  transition: transform 250ms;
  padding: 20px;
}

a.social:hover {
  transform: translateY(-2px);
}

a.youtube {
  color: #eb3223;
}

a.facebook {
  color: #4968ad;
}

a.twitter {
  color: #49a1eb;
}

a.instagram {
  color: black;
}

`;