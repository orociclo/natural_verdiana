import React, { useState, useEffect } from "react";
import { DragSwitch } from "react-dragswitch";
import "react-dragswitch/dist/index.css";
import { bool } from "prop-types";
import { StyledCartList } from "./CartList.styled";
import emailjs from "emailjs-com";
import { jsonToTable } from "./jsonToTable";
import { getTranslation } from "../../tools/tools";

import ChangeQuantityOnCartList from "../ChangeQuantityOnCartList";
import MultipleRadioButton from "../MultipleRadioButton";

import imageTrack from "../../images/track.svg";
import loader from "../../images/loader.gif";
import success_email from "../../images/success_email.png";
import error_email from "../../images/error_email.png";
import image404 from "../../images/404.jpeg";
import sendImage from "../../images/send.png";

// const serviceId = "service_zey5t9a";
// const templateId = "template_qijhze8";
// const userId = "user_uAXEH0Npsn5VXT11iZyQ8";

const serviceId = "service_ym02qoq";
const templateId = "template_trfvjun";
const userId = "user_zWF4tLMdbYzrDBJzfiOyI";

const getImageSrc = (name) => {
  const nameWithoutSpace = name.replace(/ /g, "%20");
  const rightImageURL = `https://orodom.website/products/natural_verdiana/${nameWithoutSpace}.jpeg`;
  return rightImageURL;
};

const content = {
  es: {
    nameAndSurname: "Nombre y Apellidos*",
    email: "Correo Electrónico*",
    phone: "Número de teléfono*",
    address: "Dirección*",
    note: "Nota",
    freeDelivery: "Entrega gratuita en pedidos superiores a",
    deliveryDayQuestion: "¿Qué día prefieres?",
    deliveryHourQuestion: "¿Qué hora prefieres?",
    sendOrder: "Enviar Pedido",
    yourCartIsEmpty: "Tu carro está vacío",
    orderTotal: "Total",
    price: "Precio:",
    forEach: "por cada",
    deleteProduct: "Eliminar",
    sendingOrder: "Enviando Pedido",
    successfullSendOrder: "Pedido Enviado con éxito",
    errorSendOrder: "Error al enviar el pedido",
    M: "Mar",
    X: "Mie",
    J: "Jue",
    V: "Vie",
    S: "Sab",
  },
  en: {
    nameAndSurname: "Name and Surname*",
    email: "Email*",
    phone: "Phone Number*",
    address: "Address*",
    note: "Note",
    freeDelivery: "Free delivery on orders over",
    deliveryDayQuestion: "Which day do you prefer?",
    deliveryHourQuestion: "Which hour do you prefer?",
    sendOrder: "Send Request",
    yourCartIsEmpty: "Your car is empty",
    orderTotal: "Total",
    price: "Price:",
    forEach: "for each",
    deleteProduct: "Delete",
    sendingOrder: "Sending Order",
    successfullSendOrder: "Order Shipped Successfully",
    errorSendOrder: "Error submitting order",
    M: "Tue",
    X: "Wed",
    J: "Thu",
    V: "Fri",
    S: "Sat",
  },
  fr: {
    nameAndSurname: "Prénom et Nom*",
    email: "Courrier électronique*",
    phone: "Numéro de téléphone*",
    address: "Adresse*",
    note: "Noter",
    freeDelivery: "Livraison gratuite sur les commandes supérieures à",
    deliveryDayQuestion: "Quel jour préférez-vous?",
    deliveryHourQuestion: "Quelle heure préfères-vous?",
    sendOrder: "Envoyez la commande",
    yourCartIsEmpty: "Ta voiture est vide",
    orderTotal: "Le total",
    price: "Prix:",
    forEach: "pour chaque",
    deleteProduct: "Supprimer",
    sendingOrder: "Envoi de la commande",
    successfullSendOrder: "Commande envoyée avec succès",
    errorSendOrder: "Erreur lors de l'envoi de la commande",
    M: "Mar",
    X: "Mec",
    J: "Jeu",
    V: "Ven",
    S: "Sam",
  },
  de: {
    nameAndSurname: "Name und Nachname*",
    email: "Email*",
    phone: "Telefonnummer*",
    address: "Adresse*",
    note: "Notiz",
    freeDelivery: "Kostenlose Lieferung bei Bestellungen über",
    deliveryDayQuestion: "Welchen Tag bevorzugen Sie?",
    deliveryHourQuestion: "Welche Zeit bevorzugen Sie?",
    sendOrder: "Senden Sie die Bestellung",
    yourCartIsEmpty: "Ihr Auto ist leer",
    orderTotal: "Gesamt",
    price: "Preis:",
    forEach: "Für jede",
    deleteProduct: "Entfernen",
    sendingOrder: "Bestellung senden",
    successfullSendOrder: "Bestellung erfolgreich versendeto",
    errorSendOrder: "Fehler beim Senden der Bestellung",
    M: "Die",
    X: "Mit",
    J: "Don",
    V: "Fre",
    S: "Sam",
  },
};

const CartList = ({
  openCartList,
  cartList,
  setCartList,
  total,
  setTotal,
  categoryData,
  setCategoryData,
  requestStatus,
  setRequestStatus,
  lang,
}) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [note, setNote] = useState("");
  const [day, setDay] = useState("");
  const [hour, setHour] = useState("");
  const [progressBarWidth, setProgressBarWidth] = useState(10);
  const [progressBarColor, setProgressBarColor] = useState("#f39c8e");
  const [sendRequestEnabled, setSendRequestEnabled] = useState(false);
  const [sendRequestChecked, setSendRequestChecked] = useState(false);
  // const [requestStatus, setRequestStatus] = useState(0); // 0: nothing, 1: sending, 2: sended
  const minTotalOrderForFreeDelivery = 50;

  useEffect(() => {
    setName(localStorage.getItem("name") || "");
    setEmail(localStorage.getItem("email") || "");
    setPhone(localStorage.getItem("phone") || "");
    setAddress(localStorage.getItem("address") || "");
    setNote(localStorage.getItem("note") || "");
    setDay(localStorage.getItem("day") || "");
    setHour(localStorage.getItem("hour") || "");
  }, []);

  useEffect(() => {
    calculateTotal();
  });

  const handleChangeItemQuantity = (cartItemIndex, delta) => {
    const editedCartList = [...cartList];
    if (editedCartList[cartItemIndex].quantity + delta === 0) {
      let filteredCartList = [...cartList];
      filteredCartList = filteredCartList.filter(
        (item, i) => i !== cartItemIndex
      );

      setCartList(filteredCartList);

      //Update Object List of Products Data
      const updatedCategoryData = { ...categoryData };

      updatedCategoryData[cartList[cartItemIndex].category].find(
        (item) => item.NOMBRE === cartList[cartItemIndex].name
      ).onCart = false;
      updatedCategoryData[cartList[cartItemIndex].category].find(
        (item) => item.NOMBRE === cartList[cartItemIndex].name
      ).quantity = 0;

      setCategoryData(updatedCategoryData);
    } else {
      editedCartList[cartItemIndex].quantity =
        editedCartList[cartItemIndex].quantity + delta;

      // // Update Cart
      setCartList(editedCartList);
      calculateTotal();

      //Update Object List of Products Data
      const updatedCategoryData = { ...categoryData };

      updatedCategoryData[cartList[cartItemIndex].category].find(
        (item) => item.NOMBRE === cartList[cartItemIndex].name
      ).quantity = editedCartList[cartItemIndex].quantity;

      setCategoryData(updatedCategoryData);
    }
  };

  const calculateTotal = () => {
    let total = [...cartList];
    total = total.reduce((sum, item) => {
      return sum + item.price * item.quantity;
    }, 0);
    setTotal(total.toFixed(2));
    const newProgressBarWidth =
      100 - (100 - (total * 100) / minTotalOrderForFreeDelivery);
    setProgressBarWidth(newProgressBarWidth < 100 ? newProgressBarWidth : 100);
    setProgressBarColor(newProgressBarWidth < 100 ? "#f39c8e" : "#92C03C");
    if (
      total !== 0 &&
      name !== "" &&
      email !== "" &&
      phone !== "" &&
      address !== "" &&
      day !== "" &&
      hour !== ""
    ) {
      setSendRequestEnabled(true);
      // document.getElementById("divRequestDelivery").disabled = false;
    } else {
      setSendRequestEnabled(false);
    }
  };

  const handleDelete = (index) => {
    let filteredCartList = [...cartList];
    filteredCartList = filteredCartList.filter((item, i) => i !== index);

    setCartList(filteredCartList);

    const updatedCategoryData = { ...categoryData };

    updatedCategoryData[cartList[index].category].find(
      (item) => item.NOMBRE === cartList[index].name
    ).onCart = false;
    updatedCategoryData[cartList[index].category].find(
      (item) => item.NOMBRE === cartList[index].name
    ).quantity = 0;

    setCategoryData(updatedCategoryData);
  };

  const handleName = (e) => {
    setName(e.target.value);
  };
  const handleEmail = (e) => {
    setEmail(e.target.value);
  };
  const handlePhone = (e) => {
    setPhone(e.target.value);
  };
  const handleAddress = (e) => {
    setAddress(e.target.value);
  };
  const handleNote = (e) => {
    setNote(e.target.value);
  };

  const handleRequest = async () => {
    if (sendRequestEnabled) {
      const message = jsonToTable({
        cartList,
        name,
        phone,
        email,
        address,
        note,
        day,
        hour,
        total,
      });

      localStorage.setItem("name", name);
      localStorage.setItem("email", email);
      localStorage.setItem("phone", phone);
      localStorage.setItem("address", address);
      localStorage.setItem("note", note);
      localStorage.setItem("day", day);
      localStorage.setItem("hour", hour);
      setRequestStatus(1);
      console.log("day hour ", day, hour);
      try {
        const response = await emailjs.send(
          serviceId,
          templateId,
          { name, email, message, phone, address },
          userId
        );

        if (response.status === 200) {
          console.log("Total ", total);
          const updatedCategoryData = { ...categoryData };
          console.log("CartList previous remove all ", cartList);

          cartList.map((itemCartList, i) => {
            updatedCategoryData[cartList[i].category].find(
              (itemCategory) => itemCategory.NOMBRE === itemCartList.name
            ).onCart = false;
            updatedCategoryData[cartList[i].category].find(
              (itemCategory) => itemCategory.NOMBRE === itemCartList.name
            ).quantity = 0;
            return null;
          });

          setCategoryData(updatedCategoryData);
          const emptyCartList = [];
          setCartList(emptyCartList);
          console.log("CartList after remove all ", cartList);
          setRequestStatus(2);
          setTotal(0);
          setSendRequestEnabled(false);
          setSendRequestChecked(false);
        } else {
          console.error("Failed to send email. Error: ", response);
          setRequestStatus(3);
        }
      } catch (error) {
        console.error("Failed to send email. Error: ", error);
        setRequestStatus(3);
      }
    }
  };

  return (
    <StyledCartList openCartList={openCartList}>
      {true && (
        <div id="divCartListMain">
          <div
            id="divRequestMessageMain"
            style={{ display: requestStatus !== 0 ? "flex" : "none" }}
          >
            <div id="divRequestMessageText">
              <div style={{ display: requestStatus === 1 ? "block" : "none" }}>
                <p>{content[lang].sendingOrder}</p>
                <img src={loader} alt="loader" />
              </div>
              <div style={{ display: requestStatus === 2 ? "block" : "none" }}>
                <p>{content[lang].successfullSendOrder}</p>
                <img src={success_email} alt="success_email" />
              </div>
              <div style={{ display: requestStatus === 3 ? "block" : "none" }}>
                <p>{content[lang].errorSendOrder}</p>
                <img src={error_email} alt="error_email" />
              </div>
            </div>
          </div>
          {true && (
            <div
              id="divHeaderCartListContainer"
              style={{ display: requestStatus === 0 ? "flex" : "none" }}
            >
              <div
                id="divProgressBarContainer"
                style={{
                  width: `${progressBarWidth}%`,
                  backgroundColor: `${progressBarColor}`,
                }}
              >
                <div id="divIconContainer">
                  <img id="imgTrack" src={imageTrack} alt="Track" />
                </div>
                <div id="divTextContainer">
                  {`${content[lang].freeDelivery} ${minTotalOrderForFreeDelivery} €.`}
                </div>
                <div id="divProgressBar"> </div>
              </div>
              <div id="divUserData">
                <input
                  type="text"
                  name="name"
                  id="name"
                  value={name}
                  placeholder={content[lang].nameAndSurname}
                  onChange={handleName}
                />
                <input
                  type="email"
                  name="email"
                  id="email"
                  value={email}
                  placeholder={content[lang].email}
                  onChange={handleEmail}
                />
                <input
                  type="tel"
                  name="tel"
                  id="phone"
                  value={phone}
                  placeholder={content[lang].phone}
                  onChange={handlePhone}
                />
                <input
                  type="address"
                  name="address"
                  id="address"
                  value={address}
                  placeholder={content[lang].address}
                  onChange={handleAddress}
                />
                <input
                  type="note"
                  name="note"
                  id="note"
                  value={note}
                  placeholder={content[lang].note}
                  onChange={handleNote}
                />
              </div>
              <div id="divDayAndHourSelection">
                <MultipleRadioButton
                  title={content[lang].deliveryDayQuestion + "*"}
                  optionArray={[
                    content[lang].M,
                    content[lang].X,
                    content[lang].J,
                    content[lang].V,
                    content[lang].S,
                  ]}
                  day={day}
                  setDay={setDay}
                  style={{ width: "50%" }}
                />
                <MultipleRadioButton
                  title={content[lang].deliveryHourQuestion + "*"}
                  optionArray={["17h.", "18h.", "19h.", "20h.", "21h."]}
                  hour={hour}
                  setHour={setHour}
                  style={{ width: "50%" }}
                />
              </div>
              <div id="divLaunchRequest">
                <button id="divRequestDelivery" onClick={() => handleRequest()}>
                  <img id="sendImage" src={sendImage} alt="Send Request" />
                  <label>{content[lang].sendOrder}</label>
                  <DragSwitch
                    offColor={sendRequestEnabled ? "green" : "red"}
                    disabled={!sendRequestEnabled}
                    checked={sendRequestChecked}
                    onChange={(e) => {
                      // setChecked(e);
                      handleRequest();
                    }}
                  />
                </button>
              </div>
              <div id="divTotalContainer">
                <div id="divTotalLabel">{content[lang].orderTotal} </div>
                <div id="divTotalText">{total} €</div>
              </div>
            </div>
          )}

          <div id="divBottomCartListContainer">
            <div id="divCartListContainer">
              {cartList.length === 0 && (
                <div id="divCartListEmpty">{content[lang].yourCartIsEmpty}</div>
              )}
              <div id="divItems">
                {cartList.map((item, index) => {
                  const imageSrc = getImageSrc(item.name);
                  const storedName = getTranslation(
                    item.name,
                    item.name_en,
                    item.name_fr,
                    item.name_de,
                    lang
                  );
                  return (
                    <div className="ItemContainer" key={index}>
                      <div className="imageContainer">
                        <img
                          className="itemImage"
                          // src={imageSrc2}
                          src={imageSrc}
                          alt="ImageItem"
                          onError={(e) => {
                            e.target.src = image404;
                          }}
                        />
                      </div>
                      <div className="TextContainer">
                        <div className="itemName">
                          {lang === "es" &&
                            `${item.name} 
                              ${(item.quantity * item.price).toFixed(2)}
                               €`}
                          {lang !== "es" && storedName && (
                            <div>{`${storedName} 
                            ${(item.quantity * item.price).toFixed(2)}
                             €`}</div>
                          )}
                          {/* {lang !== "es" && storedName && (
                            <div>{storedName}</div>
                          )} */}
                          {/* {(item.quantity * item.price).toFixed(2)}€ */}
                        </div>
                        <div className="itemPrice">
                          {`${content[lang].price} ${item.price} € ${content[lang].forEach} ${item.unit}`}
                        </div>
                        <div className="divQuantityContainer">
                          <ChangeQuantityOnCartList
                            handleChangeItemQuantity={(index, delta) =>
                              handleChangeItemQuantity(index, delta)
                            }
                            cartItemIndex={index}
                            cartList={cartList}
                          />
                          <div
                            className="divDelete"
                            onClick={() => handleDelete(index)}
                          >
                            {content[lang].deleteProduct}
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      )}
    </StyledCartList>
  );
};

CartList.propTypes = {
  openCartList: bool.isRequired,
};

export default CartList;
