export const jsonToTable = ({
  cartList,
  name,
  phone,
  email,
  address,
  note,
  day,
  hour,
  total,
}) => {
  const tableBodyArray = cartList.map((product, index) => {
    return `
        <tr key=${index} 
          style="border: 1px solid black";
        >
          <td style="border: 1px solid black; padding: 10px;">${
            product.category
          }</td>
          <td style="border: 1px solid black; padding: 10px;">${
            product.name
          }</td>
          <td style="border: 1px solid black; padding: 10px;">${
            product.unit
          }</td>
          <td style="border: 1px solid black; padding: 10px;">${
            product.quantity
          }</td>
          <td style="border: 1px solid black; padding: 10px; text-align: right;">${
            product.price
          }€</td>
          <td style="border: 1px solid black; padding: 10px; text-align: right;">${(
            product.quantity * product.price
          ).toFixed(2)}€</td>
          <td style="border: 1px solid black; padding: 10px; text-align: right;">
          <input type="checkbox" id="check" name="check" value="check">
          </td>
        </tr>
        `;
  });

  const tableBodyString = tableBodyArray.join("");
  const whatsappPhone = phone.includes("+34") ? phone : "+34" + phone;
  const dayText = {"Mar": "Martes", "Mie":"Miércoles", "Jue":"Jueves", "Vie":"Viernes", "Sab":"Sábado"};

  const deliveryTime = "Preferencia de reparto: " + dayText[day] + " a las " + hour;

  return `
      <div style="font-weight: normal; font-size: 14x;">
        <p>¡Hola! Nuevo pedido de:</p>
        <p>
        <span
          style="
            text-decoration: none;
            background-color: white;
            color: #92C03C;
            padding: 10px;
            border: 3px solid #92C03C;
            border-radius: 50px;
            margin-right: 20px;
          "
        >
        ${name}
        </span>
          <a 
            style="
              text-decoration: none;
              background-color: #92C03C;
              color: white;
              padding: 10px;
              border-radius: 50px;
              margin-right: 20px;
            "
            href="tel:${phone}">
            ${phone}
          </a>
          <a
            style="
              text-decoration: none;
              background-color: #92C03C;
              color: white;
              padding: 10px;
              border-radius: 50px;
              margin-right: 20px;
            " 
            href="https://api.whatsapp.com/send?phone=${whatsappPhone}&text=Hola%20${name}"
            >WhatsApp
          </a>
          <a 
          style="
              text-decoration: none;
              background-color: #92C03C;
              color: white;
              padding: 10px;
              border-radius: 50px;
            " 
          href = "mailto: ${email}"
          >${email}
          </a>
          <p
            style="
              display: flex;
              align-items: center;
              justify-content: space-evenly;
              text-decoration: none;
              background-color: white;
              color: #92C03C;
              padding: 10px;
              border: 3px solid #92C03C;
              border-radius: 50px;
              width: 500px;
              margin-top: 20px;
            " 
            >
            <a
              style="
                margin-left: 30px;
              " 
              href="https://www.google.com/maps?saddr=My+Location&daddr=${address}"
            >
              <img 
              src="https://cdn0.iconfinder.com/data/icons/maps-and-pins-3/32/connect_pins_destiny_destination_two-512.png" alt="connect pin" width="64" height="64"
              >
            </a>
            <span
              style="margin: auto;" 
            >
              ${address}
            </span>
          </p>
          <p
          style="
            display: flex;
            justify-content: center;
            text-decoration: none;
            background-color: white;
            color: #92C03C;
            padding: 10px;
            border: 3px solid #92C03C;
            border-radius: 50px;
            width: 500px;
            margin-top: 20px;
          " 
          >
          <label
            style="
              margin-left: auto;
              margin-right: auto;
            "
          >
          ${note}
          </label>
          </p>
          <p
          style="
            display: flex;
            justify-content: center;
            text-decoration: none;
            background-color: white;
            color: #92C03C;
            padding: 10px;
            border: 3px solid #92C03C;
            border-radius: 50px;
            width: 500px;
            margin-top: 20px;
          " 
          >
            <label
              style="
                margin-left: auto;
                margin-right: auto;
              "
            >
            ${deliveryTime}
            </label>
          </p>
        </p>
        <table
          style="
            text-align: center;
            width: 600px;
          "
        >
          <thead
            style="
              font-weight:bold;
              background-color: #92C03C;
              color: white;
            "
          >
            <tr
              style="
                border: 1px solid black;
                padding: 8px;
              "
            >
              <th style="padding: 10px;">CATEGORÍA</th>
              <th style="padding: 10px;">PRODUCTO</th>
              <th style="padding: 10px;">FORMATO</th>
              <th style="padding: 10px;">CANTIDAD</th>
              <th style="padding: 10px;">PRECIO</th>
              <th style="padding: 10px;">TOTAL</th>
              <th style="padding: 10px;">CHECK</th>
            </tr>
          </thead>
          <tbody>${tableBodyString}</tbody>
          <tr
              style="
                border: 1px solid black;
                padding: 8px;
              "
            >
              <td 
                align="center" 
                colspan="7" 
                style="
                  font-weight: bold;
                  font-size: 20px;
                  text-align: center;
                  background-color: #92C03C;
                  color: white;
                  padding: 10px;
                "
              >
              Total Pedido: ${total}€
              </td>
            </tr>
        </table>
        <p>¡Un saludo!</p>
      </div>
      `;
};
