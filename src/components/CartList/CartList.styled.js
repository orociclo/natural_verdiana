import styled from "styled-components";

export const StyledCartList = styled.div`
  #divCartListMain {
    position: absolute;
    z-index: 11;
    display: flex;
    flex-direction: column;
    top: 0;
    right: 0;
    width: 100%;
    height: 100vh;
    justify-content: space-between;
    background: ${({ theme }) => theme.primaryLight};
    transform: ${({ openCartList }) =>
      openCartList ? "translateY(0vh)" : "translateY(-130vh)"};
    transition: transform 0.3s ease-in-out;
    padding-bottom: 20px;
    text-align: center;
    @media (min-width: ${({ theme }) => theme.mobile}) {
      width: 576px;
    }

    #divRequestMessageMain {
      position: fixed;
      top: 50px;
      display: flex;
      flex-direction: column;
      width: 100%;
      height: 12rem;
      background: ${({ theme }) => theme.primaryLight};
      #divRequestMessageText {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: auto;
        width: 80%;
        margin: auto;
        font-size: 1rem;
        font-weight: bold;
        border-style: solid;
        border-width: 2px;
        border-color: ${({ theme }) => theme.primaryDark};
        background-color: #fff;
        border-radius: 2rem;
        text-align: center;
        p {
          margin: 5px;
          padding: 10px;
        }
        img {
          height: 100px;
          padding: 10px;
        }
      }
    }

    #divHeaderCartListContainer {
      position: fixed;
      top: 50px;
      display: flex;
      flex-direction: column;
      width: 100%;
      height: 22rem;
      background: ${({ theme }) => theme.primaryLight};
      #divTotalContainer {
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        background: ${({ theme }) => theme.primaryDark};
        color: ${({ theme }) => theme.primaryLight};
        font-size: 1.5rem;
        font-weight: bold;
        padding: 10px 0px 5px 10px;
        margin-top: 10px;
        width: 100%;
        border-top-left-radius: 50px;
        border-top-right-radius: 50px;
        #divTotalLabel {
          width: 50%;
          text-align: left;
        }
        #divTotalText {
          width: 25%;
        }
      }
      #divProgressBarContainer {
        height: 1.1rem;
        border-radius: 1.1rem;
      }
      #divIconContainer {
        position: absolute;
        margin-left: 0.5rem;
        display: flex;
        align-items: center;
        width: 30px;
      }
      #divTextContainer {
        font-size: 0.8rem;
        position: absolute;
        right: 50%;
        width: 100%;
        transform: translate(50%);
        color: #000;
      }
      #divUserData {
        display: flex;
        flex-direction: column;
        align-items: center;
        margin-left: auto;
        margin-right: auto;
        width: 93%;
        input {
          margin-top: 3px;
          font-size: 1rem;
          border-radius: 20px;
          text-align: center;
          background: ${({ theme }) => theme.primaryLight};
          border-color: ${({ theme }) => theme.primaryDark};
          border-width: 3px;
          margin: 3px;
          width: 100%;
          :select {
            background: ${({ theme }) => theme.primaryLight};
            color: #000;
          }
          :focus {
            background: ${({ theme }) => theme.primaryLight};
            color: #000;
          }
        }
      }
      #divDayAndHourSelection {
        display: flex;
        flex-direction: row;
        justify-content: space-evenly;
        width: 95%;
        margin: auto;
      }
      #divLaunchRequest {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        font-size: 1.1rem;
        font-weight: bold;
        padding-top: 3px;
        width: 93%;
        align-items: center;
        margin-left: auto;
        margin-right: auto;
        #divRequestDelivery {
          display: flex;
          justify-content: center;
          align-items: center;
          width: 100%;
          border: none;
          cursor: pointer;
          border-radius: 20px;
          background: none;
          color: #000;
          font-size: 1.2rem;
          height: 1.5rem;
          padding-top: 2px;
          cursor: pointer;
          label {
            padding: 10px;
            cursor: pointer;
          }
          img {
            width: 40px;
            cursor: pointer;
          }
          :disabled {
            background-color: darkgrey;
            cursor: not-allowed;
          }
        }
      }
      /* #switchBtn {
          position: relative;
          display: inline-block;
          width: 400px;
          height: 34px;
          font-size: 1.2rem;
          margin: auto;
        }
        #switchBtn input {
          display: none;
        }
        #slide {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: darkgrey;
          -webkit-transition: 0.4s;
          transition: 0.4s;
          padding-top: 4px;
          color: #fff;
          border-radius: 34px;
        }
        #slide:before {
          position: absolute;
          content: "";
          height: 26px;
          width: 26px;
          left: 5px;
          bottom: 4px;
          background-color: white;
          -webkit-transition: 0.4s;
          transition: 0.4s;
          border-radius: 34px;
        }
        input:checked + #slide {
          background-color:${({ theme }) => theme.primaryDark};
        }
        input:focus + #slide {
          box-shadow: 0 0 1px #01aeed;
        }
        input:checked + #slide:before {
          transform: translateX(363px);
        } */
    }

    #divBottomCartListContainer {
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      width: 100%;
      height: 100vh;
      padding-top: 25rem;
      #divCartListContainer {
        overflow-y: auto;
        overflow-x: hidden;
        /* padding-top: 5rem; */
        padding-bottom: 2rem;
        #divCartListEmpty {
          font-size: 1.5rem;
          color: ${({ theme }) => theme.primaryDark};
          margin-top: 3rem;
        }
        .ItemContainer {
          display: flex;
          justify-content: space-around;
          height: 7rem;
          margin-top: 30px;
          .imageContainer {
            display: flex;
            justify-content: center;
            height: 100%;
            width: 35%;
            .itemImage {
              height: 100%;
              width: auto;
              border-radius: 50%;
              border-style: solid;
              margin-left: 10px;
            }
          }
          .TextContainer {
            display: flex;
            flex-direction: column;
            justify-content: center;
            line-height: 2rem;
            width: 70%;
            padding-left: 5px;
            padding-right: 20px;
            .itemName {
              font-weight: bold;
              border-style: solid;
              border-width: 2px;
              border-color: ${({ theme }) => theme.primaryDark};
              background-color: #fff;
              border-radius: 2rem;
              text-align: center;
              padding-left: 15px;
              padding-right: 15px;
            }
            .itemPrice {
              text-align: center;
            }
            .divQuantityContainer {
              display: flex;
              align-items: baseline;
              justify-content: space-between;
              align-items: center;
              .divDelete {
                display: table-cell;
                text-align: center;
                vertical-align: middle;
                width: auto;
                padding-left: 1rem;
                padding-right: 1rem;
                /* box-shadow: 2px 5px rgba(8, 8, 8, 0.15); */
                font-weight: bold;
                background: ${({ theme }) => theme.primaryDark};
                color: ${({ theme }) => theme.primaryLight};
                border-radius: 2rem;
                margin: 5px;
                cursor: pointer;
              }
            }
          }
        }
      }
    }
  }
`;
