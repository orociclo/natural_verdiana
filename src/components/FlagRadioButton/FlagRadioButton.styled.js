import styled from "styled-components";

import es from "../../images/es.png";
import en from "../../images/en.png";
import fr from "../../images/fr.png";
import de from "../../images/de.png";
import loader from "../../images/loader.gif";

export const StyledFlagRadioButton = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  #divFlagRadioButtonContainer {
    display: flex;
    flex-direction: column;
    width: 100%;
    border-radius: 4rem;
    border-style: solid;
    border-width: 3px;
    border-color: ${({ theme }) => theme.primaryDark};
    padding: 5px;
    margin-left: 5px;
    margin-right: 5px;
    #labelTitleFlagRadioButton {
      font-size: 0.85rem;
    }
    form {
      display: flex;
      width: 100%;
      justify-content: space-around;
      align-items: center;
      height: 4rem;
      div {
        input[type="radio"] {
          display: none;
        }

        input[type="radio"] + label span {
          display: inline-block;
          background-repeat: no-repeat;
          width: 19px;
          height: 19px;
          vertical-align: middle;
          background-size: contain;
          cursor: pointer;
        }
        input[type="radio"]:checked + label span {
          width: 39px;
          height: 39px;
          background-size: contain;
        }
        #es {
          background: url(${es});
          background-size: contain;
        }
        #en {
          background: url(${en});
          background-size: contain;
        }
        #fr {
          background: url(${fr});
          background-size: contain;
        }
        #de {
          background: url(${de});
          background-size: contain;
        }
        #loader {
          background: url(${loader});
          background-size: contain;
        }
      }
    }
  }
`;
