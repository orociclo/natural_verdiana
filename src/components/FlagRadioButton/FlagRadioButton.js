import React, { useState, useEffect } from "react";
// import useTranslate from "../../hooks/useTranslate";
import { StyledFlagRadioButton } from "./FlagRadioButton.styled";

const FlagRadioButton = ({ lang, setLang, groupCategories }) => {
  const [clickOnFlag, setClickOnFlag] = useState(false);
  const [showingLoaderEn, setShowingLoaderEn] = useState(false);
  const [showingLoaderFr, setShowingLoaderFr] = useState(false);
  const [showingLoaderDe, setShowingLoaderDe] = useState(false);
  const showingLoaderArray = [
    false,
    showingLoaderEn,
    showingLoaderFr,
    showingLoaderDe,
  ];
  const setShowingLoaderArray = [
    false,
    setShowingLoaderEn,
    setShowingLoaderFr,
    setShowingLoaderDe,
  ];
  const flagStringArray = ["es", "en", "fr", "de"];

  const executeOneTranslation = async ({ text, source_lang, target_lang }) => {
    return await getTranslationAndLocalStore({
      text,
      source_lang,
      target_lang,
    });
  };

  const executeTranslationCategoryArray = async (groupCategories) => {
    // Prepare array of translation category promises
    const promiseCategoryArray = groupCategories.map((category) => {
      return executeOneTranslation({
        text: category.label,
        source_lang: "es",
        target_lang: lang,
      });
    });
    // Launch category promises
    try {
      await Promise.all(promiseCategoryArray);
    } catch {
      throw Error("Promise failed");
    }
  };

  const executeTranslationOptionArray = async (groupCategories) => {
    // Prepare arrys of translation options promises
    const promiseOptionArray = [];
    groupCategories.map((category) => {
      category.options.map((option) => {
        promiseOptionArray.push(
          executeOneTranslation({
            text: option.label,
            source_lang: "es",
            target_lang: lang,
          })
        );
        if (option.note.length > 0) {
          promiseOptionArray.push(
            executeOneTranslation({
              text: option.note,
              source_lang: "es",
              target_lang: lang,
            })
          );
        }
        return null;
      });
      return null;
    });
    try {
      console.log("Launching Options promises....");
      await Promise.all(promiseOptionArray);
    } catch {
      throw Error("Promise failed");
    }
  };

  // useEffect(() => {
  //   if (clickOnFlag) {
  //     setClickOnFlag(false);
  //     const translateLanguage = "translate_" + lang;
  //     const getItemLocalStorage = localStorage.getItem(translateLanguage);
  //     if (lang.toLowerCase() !== "es") {
  //       console.log("Changing language to....", lang);
  //       const indexShowingLoader = flagStringArray.indexOf(lang);
  //       setShowingLoaderArray[indexShowingLoader](true);
  //       // executeTranslationCategoryArray(groupCategories);
  //       // executeTranslationOptionArray(groupCategories);
  //       console.log("getItemLocalStorage ", getItemLocalStorage);
  //       if (getItemLocalStorage === null) {
  //         setTimeout(() => {
  //           const languageClicked = "translate_" + lang;
  //           localStorage.setItem(languageClicked, "true");
  //           console.log("End Promise.all ......... res: ");
  //           setShowingLoaderArray[indexShowingLoader](false);
  //         }, 4000);
  //       } else {
  //         setTimeout(() => {
  //           console.log("End Promise.all ......... res: ");
  //           setShowingLoaderArray[indexShowingLoader](false);
  //         }, 500);
  //       }
  //     }
  //   }
  // }, [lang]);

  const getTranslationAndLocalStore = async ({
    text = "",
    source_lang,
    target_lang,
  }) => {
    return new Promise((resolve, reject) => {
      const itemLocalStorageKey = text.replace(/ /g, "_") + "_" + target_lang;
      const getItemLocalStorage = localStorage.getItem(itemLocalStorageKey);
      if (getItemLocalStorage) {
        resolve(getItemLocalStorage);
        // console.log("Succesful getItemLocalStorage .........", getItemLocalStorage)
      } else {
        const data = JSON.stringify({
          text: "Sopa de patatas",
          source: "es",
          target: "en",
        });

        const xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
          if (this.readyState === this.DONE) {
            console.log("--------------------->" + this.responseText);
          }
        });

        xhr.open("POST", "https://deepl-translator.p.rapidapi.com/translate/");
        xhr.setRequestHeader("content-type", "application/json");
        xhr.setRequestHeader(
          "X-RapidAPI-Key",
          "19536e1418msh809b75768b39c78p123d79jsnfca5564df23a"
        );
        xhr.setRequestHeader(
          "X-RapidAPI-Host",
          "deepl-translator.p.rapidapi.com"
        );

        xhr.send(data);

        // const xhr = new XMLHttpRequest();
        // const url = "https://api-free.deepl.com/v2/translate";
        // xhr.open("POST", url);
        // xhr.setRequestHeader(
        //   "Content-Type",
        //   "application/x-www-form-urlencoded"
        // );
        // xhr.onreadystatechange = () => {
        //   if (xhr.readyState === 4 && xhr.status === 200) {
        //     const responseText = JSON.parse(xhr.responseText).translations[0]
        //       .text;
        //     if (responseText !== "") {
        //       localStorage.setItem(itemLocalStorageKey, responseText);
        //       console.log(
        //         "getTranslationAndLocalStore added to localStorage: ",
        //         itemLocalStorageKey,
        //         ":",
        //         responseText
        //       );
        //       resolve(true);
        //     } else {
        //       localStorage.setItem(itemLocalStorageKey, text);
        //     }
        //   } else {
        //     localStorage.setItem(itemLocalStorageKey, text + "*");
        //     reject(xhr.statusText);
        //   }
        // };
        // const data = `auth_key=8c8d4d76-c19c-d2fd-6429-015176fb6785:fx&source_lang=${source_lang}&text=${text}&target_lang=${target_lang}`;
        // xhr.send(data);
      }
    });
  };

  const handleOnChange = (e) => {
    setLang(e.target.value);
    setClickOnFlag(true);
  };

  return (
    <StyledFlagRadioButton id="divFlagRadioButtonComponent">
      <div id="divFlagRadioButtonContainer">
        <form id="formFlagRadioButton">
          {flagStringArray.map((flag, index) => {
            return (
              <div className="divOptionItem" key={index}>
                <input
                  type="radio"
                  id={`flag${index}`}
                  name={flag}
                  value={flag}
                  onChange={handleOnChange}
                  checked={flag === lang}
                />
                <label htmlFor={`flag${index}`}>
                  {!showingLoaderArray[index] && (
                    <span id={flagStringArray[index]}></span>
                  )}
                  {showingLoaderArray[index] && <span id="loader"></span>}
                </label>
              </div>
            );
          })}
        </form>
      </div>
    </StyledFlagRadioButton>
  );
};

export default FlagRadioButton;
