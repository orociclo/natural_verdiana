import React, { useState } from "react";
import useIntersectionObserver from "../../hooks/useIntersectionObserver";
import useImage from "../../hooks/useImage";

import { StyledIntersectionObserverImage } from "./IntersectionObserverImage.styled";

import image404 from "../../images/404.jpeg";
import loader from "../../images/loader.gif";

const getImageSrc = (name) => {
  const nameWithoutSpace = name.replace(/ /g, "%20");
  // console.log("Changed ", name, nameWithoutSpace);
  const rightImageURL = `https://orodom.website/products/natural_verdiana/${nameWithoutSpace}.jpeg`;
  return rightImageURL;
};

const IntersectionObserverImage = ({ name, imageURL, height, width, alt }) => {
  const ref = React.useRef();
  const [isVisible, setIsVisible] = useState(false);
  const imageSrc3 = getImageSrc(name);
  const { hasLoaded, hasError } = useImage(imageSrc3);

  useIntersectionObserver({
    target: ref,
    onIntersect: ([{ isIntersecting }], observerElement) => {
      if (isIntersecting) {
        setIsVisible(true);
        observerElement.unobserve(ref.current);
      }
    },
  });

  const aspectRatio = (height / width) * 100;

  return (
    <StyledIntersectionObserverImage
      ref={ref}
      style={{ paddingBottom: `${aspectRatio}%` }}
      id="divIntersectionObserverImage"
    >
      {isVisible && (
        <div id="divIntersectionObserverImage">
          {/* <img className="image" src={imageSrc} alt={alt} draggable="false" /> */}
          {!hasLoaded && (
            <img className="image" src={loader} alt={"loader"} draggable="false" />
          )}
           {hasError && (
            <img className="image" src={image404} alt={"error404"} draggable="false" />
          )}
          {hasLoaded && (
            <img
              className="image"
              // src={imageSrc2}
              src={imageSrc3}
              alt={alt}
              draggable="false"
              onError={(e) => {
                e.target.src = image404;
              }}
            />
          )}
        </div>
      )}
    </StyledIntersectionObserverImage>
  );
};

export default IntersectionObserverImage;
