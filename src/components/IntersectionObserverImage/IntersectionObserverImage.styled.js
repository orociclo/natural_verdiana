import styled from "styled-components";

export const StyledIntersectionObserverImage = styled.div`
  position: relative;
  overflow: hidden;
  /* background-color: green; */
  /* background: rgba(0, 0, 0, 0.05); */
  #divIntersectionObserverImage {
    padding-top: 0.5rem;
  }
  .image {
    padding: 30px;
    position: absolute;
    border-radius: 50%;
    width: 95%;
    height: 92%;
    left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
    border-style: solid;
    border-width: 3px;
    background-color: white;
  }
`;
