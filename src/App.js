import React, { useState, useRef, useEffect } from "react";
import Papa from "papaparse";
import { ThemeProvider } from "styled-components";
import { useOnClickOutside } from "./hooks";
import { GlobalStyles } from "./global";
import { theme } from "./theme";
import { Menu } from "./components";
import Header from "./components/Header";
import CartList from "./components/CartList";
import BackBlackBackground from "./components/BackBlackBackground";

import About from "./Pages/About";
import Order from "./Pages/Order";
import Contact from "./Pages/Contact";

import loader from "./images/loader.gif";

function App() {
  const [openMenu, setOpenMenu] = useState(false);
  const [openCartList, setOpenCartList] = useState(false);
  const [pageOpen, setPageOpen] = useState(1);
  const [cartList, setCartList] = useState([]);
  const [total, setTotal] = useState(0);
  const [categoryData, setCategoryData] = useState();
  const [categories, setCategories] = useState();
  const [groupCategories, setGroupCategories] = useState();
  const [requestStatus, setRequestStatus] = useState(0); // 0: nothing, 1: sending, 2: sended

  const [lang, setLang] = useState("es");

  const node = useRef();
  const menuId = "main-menu";

  useOnClickOutside(node, () => setOpenMenu(false));

  useEffect(() => {
    const navLang = window.navigator.language.slice(0, 2);
    if (navLang !== "es") {
      setLang(navLang);
    }

    const configPapaParse = {
      header: true,
    };

    const response = fetch(
      "https://orodom.website/products/natural_verdiana/products.csv"
    )
      .then((fetchResponse) => fetchResponse.text())
      .then((binaryData) => Papa.parse(binaryData, configPapaParse))
      .catch((err) => console.log(err));

    response.then((result) => {
      const visibleData = result.data.filter((item) => {
        if (item.VISIBLE === "S" || item.VISIBLE === "s" ) return item;
      });
      console.log("visibleData", visibleData);
      const sheetData = visibleData;
      sheetData.map((item) => {
        item.onCart = false;
        item.categorySelected = false;
        item.quantity = 0;
        return item;
      });
      console.log("sheetData ", visibleData);
      //Join on Categories
      const objectListArraysCategories = visibleData.reduce((r, a) => {
        r[a.CATEGORIA] = [...(r[a.CATEGORIA] || []), a];
        return r;
      }, {});
      //Object to Array
      setCategoryData(objectListArraysCategories);
      console.log("objectListArraysCategories ", objectListArraysCategories);
      // Simple array of categories
      const arrayCategories = Object.values(objectListArraysCategories).map(
        (data) => {
          console.log("data ", data)
          const item = {name: data[0].CATEGORIA, name_en: data[0].CATEGORIA_EN, name_fr: data[0].CATEGORIA_FR, name_de: data[0].CATEGORIA_DE }
          return item;
        }
      );
      setCategories(arrayCategories);
      // Fill array to search by typing
      const arrayGroupCategories = Object.values(
        objectListArraysCategories
      ).map((category, categoryIndex) => {
        return {
          label: category[0].CATEGORIA,
          options: category.map((product, productIndex) => {
            return {
              value: product.NOMBRE,
              label: product.NOMBRE,
              category: product.CATEGORIA,
              note: product.NOTA,
              categoryIndex: categoryIndex,
              productIndex: productIndex,
              category_en: product.CATEGORIA_EN,
              name_en: product.NOMBRE_EN,
              category_fr: product.CATEGORIA_FR,
              name_fr: product.NOMBRE_FR,
              category_de: product.CATEGORIA_DE,
              name_de: product.NOMBRE_DE              
            };
          }),
        };
      });
      console.log("arrayGroupCategories ", arrayGroupCategories);
      setGroupCategories(arrayGroupCategories);
    });

    return;
  }, []);

  return (
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        {(openMenu || openCartList) && (
          <BackBlackBackground setOpenCartList={setOpenCartList} />
        )}
        <div id="divMenu" ref={node}>
          <Header
            openMenu={openMenu}
            setOpenMenu={setOpenMenu}
            openCartList={openCartList}
            setOpenCartList={setOpenCartList}
            cartList={cartList}
            setRequestStatus={setRequestStatus}
          />
          <Menu
            openMenu={openMenu}
            setOpenMenu={setOpenMenu}
            setPageOpen={setPageOpen}
            id={menuId}
            lang={lang}
            setLang={setLang}
            groupCategories={groupCategories}
          />
          <CartList
            openCartList={openCartList}
            cartList={cartList}
            setCartList={setCartList}
            total={total}
            setTotal={setTotal}
            categoryData={categoryData}
            setCategoryData={setCategoryData}
            requestStatus={requestStatus}
            setRequestStatus={setRequestStatus}
            lang={lang}
          />
        </div>
        <div id="divContentContainer">
          {pageOpen === 0 && <About lang={lang} />}
          {pageOpen === 1 && categories && (
            <Order
              categoryData={categoryData}
              setCategoryData={setCategoryData}
              categories={categories}
              setCartList={setCartList}
              cartList={cartList}
              groupCategories={groupCategories}
              lang={lang}
            />
          )}
          {pageOpen === 2 && <Contact />}
          {!categories && (
            <div id="divLoader">
              <img id="imgLoader" src={loader} alt="loader" />
            </div>
          )}
        </div>
      </ThemeProvider>
    </React.StrictMode>
  );
}

export default App;
