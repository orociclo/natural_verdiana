// const getTranslation_sos = (text, target_lang) => {
//   const objectRememberedKey = text.replace(/ /g, "_") + "_" + target_lang;
//   const retrievedTranslation = localStorage.getItem(objectRememberedKey);
//   if (retrievedTranslation) {
//     return retrievedTranslation;
//   } else {
//     return null;
//   }
// };

const getTranslation = (text_es, text_en, text_fr, text_de, target_lang) => {
  if (target_lang === "en") {
    return text_en;
  } else if (target_lang === "fr") {
    return text_fr;
  } else if (target_lang === "de") {
    return text_de;
  } else {
    return text_es;
  }
};

export { getTranslation };
