import styled from "styled-components";

export const StyledAbout = styled.div`
  #divAboutMain {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding-left: 20px;
    padding-right: 20px;
    width: 100%;
    height: auto;
    font-size: 1.01rem;
    .wise-iframe-wrapper {
      width: 90vw;
      height: 50vw;
    }
    .wise-iframe-wrapper iframe,
    .wise-iframe-wrapper object,
    .wise-iframe-wrapper embed {
      height: 100%;
      width: 100%;
    }
    img {
      width: 100%;
      height: auto;
    }
    p {
      margin: 0 0 15px;
      box-sizing: border-box;
      color: #333;
      line-height: 1.6;
      text-align: left;
    }
    h2 {
      text-align: center;
    }
    #divCommitmentContainer {
      display: flex;
      flex-direction: column;
      img {
        /* width: 33%; */
      }
      .divCommitmentCard {
        width: 33%;
        padding-left: 1rem;
        padding-right: 1rem;
        margin: 1rem;
        p {
          text-align: center;
        }
      }
      #divCommitmentText {
        text-align: center;
        width: 100%;
      }
      #divCommitmentFirstRow {
        display: flex;
        flex-direction: row;
        justify-content: center;
        @media (max-width: ${({ theme }) => theme.mobile}) {
          flex-direction: column;
          div {
            width: 100%;
          }
        }
      }
      #divCommitmentSecondRow {
        display: flex;
        flex-direction: row;
        justify-content: center;
        @media (max-width: ${({ theme }) => theme.mobile}) {
          flex-direction: column;
          div {
            width: 100%;
          }
        }
      }
    }
  }
`;
