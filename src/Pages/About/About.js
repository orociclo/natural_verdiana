import React from "react";
import { StyledAbout } from "./About.styled";
import proveemos from "../../images/proveemos.png";
import atencion from "../../images/atencion.png";
import entrega from "../../images/entrega.png";
import conocenos from "../../images/conocenos.png";
import respeto from "../../images/respeto.png";

const content = {
  es: {
    title: "La esencia",
    subTitle: "Alimentos naturales para la salud y el bienestar",
    ourCommintment: "NUESTRO COMPROMISO",
    weProvide: "PROVEEMOS",
    moreThan150: "MÁS DE 150 MARCAS",
    customerSupport: "ATENCIÓN AL CLIENTE",
    customized: "PERSONALIZADA",
    deliveries:"ENTREGAS EN",
    deliveriesTime:"24/48H",
    weKnow:"CONOCEMOS EL SECTOR",
    weWork: "Y TRABAJAMOS CON LAS",
    bestBrands: "MEJORES MARCAS",
    respect: "RESPETO POR EL TRABAJO",
    fromFarmer: "DESDE AGRICULTOR Y EL",
    proximity: "PRODUCTO DE PROXIMIDAD",
  },
  en: {
    title: "The essence",
    subTitle: "Natural foods for health and wellness",
    ourCommintment: "OUR COMMITMENT",
    weProvide: "WE PROVIDE",
    moreThan150: "MORE THAN 150 BRANDS",
    customerSupport: "CUSTOMER SUPPORT",
    customized: "CUSTOMIZED",
    deliveries:"DELIVERIES IN",
    deliveriesTime:"24/48H",
    weKnow:"WE KNOW THE SECTOR",
    weWork: "AND WE WORK WITH THE",
    bestBrands: "BEST BRANDS",
    respect: "RESPECT FOR WORK",
    fromFarmer: "FROM FARMER AND THE",
    proximity: "PROXIMITY PRODUCT",
  },
  fr: {
    title: "L'essence",
    subTitle: "Aliments naturels pour la santé et le bien-être",
    ourCommintment: "NOTRE ENGAGEMENT",
    weProvide: "NOUS FOURNISSONS",
    moreThan150: "PLUS DE 150 MARQUES",
    customerSupport: "ATTENTION AU CLIENT",
    customized: "PERSONNALISÉ",
    deliveries:"LIVRAISONS EN",
    deliveriesTime:"24/48H",
    weKnow:"NOUS CONNAISSONS LE SECTEUR",
    weWork: "ET NOUS TRAVAILLONS AVEC",
    bestBrands: "MEILLEURES MARQUES",
    respect: "RESPECT DU TRAVAIL",
    fromFarmer: "DE L'AGRICULTEUR ET DE LA",
    proximity: "PRODUIT DE PROXIMITÉ",
  },
  de: {
    title: "Die Essenz",
    subTitle: "Natürliche Lebensmittel für Gesundheit und Wohlbefinden",
    ourCommintment: "UNSER ENGAGEMENT",
    weProvide: "WIR BIETEN AN",
    moreThan150: "MEHR ALS 150 MARKEN",
    customerSupport: "KUNDENDIENST",
    customized: "MASSGESCHNEIDERT",
    deliveries:"LIEFERUNGEN IN",
    deliveriesTime:"24/48H",
    weKnow:"WIR KENNEN DIE BRANCHE",
    weWork: "UND WIR ARBEITEN MIT DEM",
    bestBrands: "BESTE MARKEN",
    respect: "RESPEKT VOR DER ARBEIT",
    fromFarmer: "VOM BAUER UND DER",
    proximity: "PROXIMITY-PRODUKT",
  },
};

const About = ({ lang }) => {
  return (
    <StyledAbout>
      <div id="divAboutMain">
        <h1>{content[lang].title}</h1>
        <div className="wise-iframe-wrapper">
          <iframe
            src="https://www.youtube.com/embed/XU8no8wTZ-w"
            title="YouTube video player"
            frameborder="0"
            controls="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </div>
        <h2>{content[lang].subTitle}</h2>
        <div id="divCommitmentContainer">
          <div id="divCommitmentText">
            <h2>{content[lang].ourCommintment}</h2>
          </div>
          <div id="divCommitmentFirstRow">
            <div className="divCommitmentCard">
              <img src={proveemos} alt="proveemos" />
              <p>{content[lang].weProvide}</p>
              <p>{content[lang].moreThan150}</p>
            </div>
            <div className="divCommitmentCard">
              <img src={atencion} alt="atencion" />
              <p>{content[lang].customerSupport}</p>
              <p>{content[lang].customized}</p>
            </div>
            <div className="divCommitmentCard">
              <img src={entrega} alt="entrega" />
              <p>{content[lang].deliveries}</p>
              <p>{content[lang].deliveriesTime}</p>
            </div>
          </div>
          <div id="divCommitmentSecondRow">
            <div className="divCommitmentCard">
              <img src={conocenos} alt="conocenos" />
              <p>{content[lang].weKnow}</p>
              <p>{content[lang].weWork}</p>
              <p>{content[lang].bestBrands}</p>
            </div>
            <div className="divCommitmentCard">
              <img src={respeto} alt="respeto" />
              <p>{content[lang].respect}</p>
              <p>{content[lang].fromFarmer}</p>
              <p>{content[lang].proximity}</p>
            </div>
          </div>
        </div>
      </div>
    </StyledAbout>
  );
};

export default About;
