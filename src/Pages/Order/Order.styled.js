import styled from "styled-components";

export const StyleOrder = styled.div`
  height: 100%;
  #divSelectionAndProductSwiperContainer {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: center;
    height: 100%;
    .divImageContainer {
      position: relative;
      height: 100%;
      #divName {
        position: absolute;
        z-index: 2;
        top: 0px;
        left: 50%;
        transform: translate(-50%, 0%);
        text-align: center;
        font-size: ${({ theme }) => theme.primaryFontSize};
        font-weight: bolder;
        color: ${({ theme }) => theme.primaryDark};
        width: 97%;
        border-style: solid;
        border-width: 3px;
        border-color: ${({ theme }) => theme.primaryDark};
        background: white;
        border-radius: 2rem;
        padding: 5px 10px 5px 10px;
        @media (min-width: ${({ theme }) => theme.mobile}) {
          font-size: ${({ theme }) => theme.smallFontSize};
        }
      }
      #divBottom {
        position: absolute;
        text-align: center;
        margin-top: 60%;
        top: 42%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 4;
        width: 97%;
      }
      #divOriginAndPrice {
        text-align: center;
        z-index: 4;
        font-weight: bolder;
        color: ${({ theme }) => theme.primaryDark};
        width: 100%;
        font-size: ${({ theme }) => theme.primaryFontSize};
        border-style: solid;
        border-width: 3px;
        border-color: ${({ theme }) => theme.primaryDark};
        background: white;
        border-radius: 2rem;
        padding: 5px 10px 5px 10px;
        @media (min-width: ${({ theme }) => theme.mobile}) {
          font-size: ${({ theme }) => theme.smallFontSize};
        }
      }

      #divImageCartContainer {
        width: 100%;
        text-align: center;
        z-index: 4;
        font-weight: bolder;
        color: black;
        padding-top: 3px;
        margin-bottom: 10px;
        #divImageToCart {
          display: block;
          height: 50px;
          width: 50px;
          margin-left: auto;
          margin-right: auto;
          padding: 5px;
          filter: invert(1);
          /* box-shadow: 2px 5px #888; */
          background: ${({ theme }) => theme.primaryDarkInverter};
          color: ${({ theme }) => theme.primaryLight};
          border-radius: 50%;
          cursor: pointer;
          :active {
            box-shadow: none;
            transform: translate(2px, 5px);
          }
          img {
            width: 95%;
          }
        }
        #divImageOnCart {
          display: block;
          height: 50px;
          width: 50px;
          margin-left: auto;
          margin-right: auto;
        }
        #divChangeQuantityMain {
          display: flex;
          flex-direction: row;
          justify-content: center;

          #divLess {
            padding-left: 10px;
            padding-right: 10px;
            font-size: 2rem;
            border-top-left-radius: 50%;
            border-bottom-left-radius: 50%;
            background: ${({ theme }) => theme.primaryDark};
            color: ${({ theme }) => theme.primaryLight};
            cursor: pointer;
            /* box-shadow: 2px 5px rgb(8 8 8 / 15%); */
          }
          #divMore {
            padding-left: 10px;
            padding-right: 10px;
            font-size: 2rem;
            border-top-right-radius: 50%;
            border-bottom-right-radius: 50%;
            background: ${({ theme }) => theme.primaryDark};
            color: ${({ theme }) => theme.primaryLight};
            cursor: pointer;
            /* box-shadow: 2px 5px rgb(8 8 8 / 15%); */
          }
          #divValue {
            padding-left: 10px;
            padding-right: 10px;
            font-size: 2rem;
            background: ${({ theme }) => theme.primaryLight};
            color: ${({ theme }) => theme.primaryDark};
            width: 4rem;
          }
        }
        #divNotes {
          position: absolute;
          text-align: center;
          font-weight: bolder;
          color: ${({ theme }) => theme.primaryDark};
          width: 100%;
          font-size: ${({ theme }) => theme.primaryFontSize};
          border-style: solid;
          border-width: 3px;
          border-color: ${({ theme }) => theme.primaryDark};
          background: white;
          border-radius: 50px;
          padding: 5px 10px 5px 10px;
          margin-top: 20px;
          @media only screen and (max-height: 480px) {
            display: none;
          }
          @media only screen and (max-width: 760px) {
            display: none;
          }
        }
      }
    }
  }
`;
