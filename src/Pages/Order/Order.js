import React, { useState, useEffect } from "react";
import { getTranslation } from "../../tools/tools";
import { StyleOrder } from "./Order.styled";
import ProductSwiper from "../../components/ProductSwiper";
import IntersectionObserverImage from "../../components/IntersectionObserverImage";
import SelectCategorySearchProduct from "../../components/SelectCategorySearchProduct";

import loader from "../../images/loader.gif";

const content = {
  es: {
    forEach: "por cada",
  },
  en: {
    forEach: "for each",
  },
  fr: {
    forEach: "pour chaque",
  },
  de: {
    forEach: "für jeden",
  },
};

const Order = ({
  categoryData,
  setCategoryData,
  categories,
  setCartList,
  cartList,
  groupCategories,
  lang,
}) => {
  console.log("categories.............. ", categories)
  const [indexCategorySelected, setIndexCategorySelected] = useState(null);
  const [indexProductSelected, setIndexProductSelected] = useState(null);
  const [indexProductToCart, setIndexProductToCart] = useState(null);
  const [htmlProductList, setHtmlProductList] = useState([]);

  // Fill htmlProductList when indexCategorySelected !==0

  useEffect(() => {
    if (indexCategorySelected !== null) {
      let valuesSelectedCategory = null;

      if (indexProductToCart !== null) {
        //Update Object List of Products Data When a Product is added to cart
        const keysProductToUpdate =
          Object.keys(categoryData)[indexCategorySelected];

        const valuesProductToUpdate =
          Object.values(categoryData)[indexCategorySelected][
            indexProductToCart
          ];

        const productToUpdate = {};
        productToUpdate[keysProductToUpdate] = valuesProductToUpdate;
        const updatedCategoryData = { ...categoryData, productToUpdate };

        valuesSelectedCategory =
          Object.values(updatedCategoryData)[indexCategorySelected];
      } else {
        // Take values of selected category withoutChange (onCart) to fill listHTML
        const updatedCategoryData = { ...categoryData };
        valuesSelectedCategory =
          Object.values(updatedCategoryData)[indexCategorySelected];
      }

      //Fill arrayHtmlProductList
      const updatedHtmlProductList = valuesSelectedCategory.map(
        (productItem, index) => {
          const storedName = getTranslation(productItem.NOMBRE, productItem.NOMBRE_EN, productItem.NOMBRE_FR, productItem.NOMBRE_DE, lang);
          const storedNote = getTranslation(productItem.NOTA, lang);
          return (
            <div className="divImageContainer">
              <div id="divName">
                {lang === "es" && productItem.NOMBRE}
                {lang !== "es" && storedName && <div>{storedName}</div>}
              </div>
              <div id="divBottom">
                <div id="divOriginAndPrice">
                  <div>
                    {`${productItem.PRECIO}€ ${content[lang].forEach} ${productItem.FORMATO}`}
                  </div>
                </div>
                <div id="divImageCartContainer">
                  <div id="divChangeQuantityMain">
                    <div
                      id="divLess"
                      onClick={() =>
                        handleAddToCart({ productItem, index, delta: -1 })
                      }
                    >
                      -
                    </div>
                    <div id="divValue">{productItem.quantity}</div>
                    <div
                      id="divMore"
                      onClick={() =>
                        handleAddToCart({ productItem, index, delta: 1 })
                      }
                    >
                      +
                    </div>
                  </div>
                  {productItem.NOTA.length > 0 && (
                    <div id="divNotes">
                      {lang === "es" && productItem.NOTA}
                      {lang !== "es" && storedNote && <div>{storedNote}</div>}
                    </div>
                  )}
                </div>
              </div>
              <IntersectionObserverImage
                name={productItem.NOMBRE}
                imageURL={productItem.IMAGEN}
                height={300}
                width={300}
                alt={"Product Image"}
              />
            </div>
          );
        }
      );
      setHtmlProductList(updatedHtmlProductList);
    }
  }, [categoryData, cartList, lang]);

  const handleAddToCart = ({ productItem, index, delta }) => {
    // When quantity of items is cero, remove item from cartList
    if (productItem.quantity + delta === 0) {
      let filteredCartList = [...cartList];
      filteredCartList = filteredCartList.filter(
        (item) => item.name !== productItem.NOMBRE
      );
      setCartList(filteredCartList);
    } else {
      // Add item to Cart
      setIndexProductToCart(index);
      const itemToAdd = {
        category: productItem.CATEGORIA,
        name: productItem.NOMBRE,
        price: parseFloat(
          productItem.PRECIO.replace(",", ".").replace(" ", "")
        ),
        quantity: productItem.quantity + delta,
        unit: productItem.FORMATO,
        imageURL: productItem.IMAGEN,
      };

      // CartList without previous itemToAdd
      let filteredCartList = [...cartList];
      filteredCartList = filteredCartList.filter(
        (item) => item.name !== productItem.NOMBRE
      );

      filteredCartList = [...filteredCartList, itemToAdd];

      // Update Cart
      setCartList(filteredCartList);
    }
    if (productItem.quantity + delta > -1) {
      //Update Object List of Products Data
      const keysProductToUpdate =
        Object.keys(categoryData)[indexCategorySelected];

      const valuesProductToUpdate =
        Object.values(categoryData)[indexCategorySelected][index];

      // Change value onCart
      valuesProductToUpdate.onCart = true;
      valuesProductToUpdate.quantity = valuesProductToUpdate.quantity + delta;

      const productToUpdate = {};
      productToUpdate[keysProductToUpdate] = valuesProductToUpdate;
      const updatedCategoryData = { ...categoryData, productToUpdate };

      setCategoryData(updatedCategoryData);
    }
  };

  const handleOnChangeCategory = (categoryIndex, productIndex = null) => {
    setIndexCategorySelected(categoryIndex);
    setIndexProductSelected(productIndex);
    const categoryDataItemsSelected = { ...categoryData };

    categoryDataItemsSelected[
      Object.values(categoryDataItemsSelected)[categoryIndex][0].CATEGORIA
    ].map((item) => {
      item.categorySelected = true;
      return item;
    });

    setCategoryData(categoryDataItemsSelected);
  };

  return (
    <StyleOrder id="divOrderMain">
      {categories && (
        <div id="divSelectionAndProductSwiperContainer">
          {categories && (
            <SelectCategorySearchProduct
              handleOnChangeCategory={handleOnChangeCategory}
              categories={categories}
              setCategoryData={setCategoryData}
              groupCategories={groupCategories}
              lang={lang}
            />
          )}
          {categories && (
            <ProductSwiper
              htmlProductList={htmlProductList}
              indexCategorySelected={indexCategorySelected}
              indexProductSelected={indexProductSelected}
            />
          )}
        </div>
      )}
      {!categories && (
        <div id="divLoader">
          <img id="imgLoader" src={loader} alt="loader" />
        </div>
      )}
    </StyleOrder>
  );
};

export default Order;
