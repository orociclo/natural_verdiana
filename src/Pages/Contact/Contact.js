import React from "react";
import { StyledContact } from "./Contact.styled";
import SocialFollow from "../../components/SocialFollow";

const Contact = ({ ...props }) => {
  return (
    <StyledContact {...props}>
      <div id="divAbout">
        <div className="divBoxAbout">
          <h2>Dónde estamos</h2>
          <h3>Calle Sierra de Alcaraz</h3>
          <h3>Plaza de Abastos</h3>
          <h3>30860 Pto. de Mazarrón</h3>
          <a href="https://www.google.com/maps/dir/?api=1&origin=34.1030032,-118.41046840000001&destination=34.059808,-118.368152">Ver en mapa</a>
          <p></p>
          <a href="https://www.google.com/maps/dir/?api=1&origin=My+Location&destination=37.566504, -1.261284">Camino hasta alli</a>
        </div>
        <div className="divBoxAbout">
          <h2>Teléfono</h2>
          <h3>610 030 635</h3>
          <a href="tel:+34610030635">Llamar</a>
        </div>
        <div className="divBoxAbout">
          <h2>Whatsapp</h2>
          <h3>610 030 635</h3>
          <a href="https://api.whatsapp.com/send?phone=+34610030635">Enviar mensaje</a>
        </div>
        <div className="divBoxAbout">
          <h2>Correo Electrónico</h2>
          <a href="mailto:naturalverdiana@gmail.com?" subject="Contacto Natural Verdiana">naturalverdiana@gmail.com</a>
        </div>
        <SocialFollow />
      </div>
    </StyledContact>
  );
};

export default Contact;
