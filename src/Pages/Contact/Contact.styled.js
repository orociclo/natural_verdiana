import styled from "styled-components";

export const StyledContact = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;

  #divAbout {
    display: flex;
    flex-direction: column;
    padding: 30px;
    align-items: center;
    height: 100%;
    text-align: center;
    h2,
    h3 {
      margin: 0.5rem;
    }
  }
  .divBoxAbout {
    display: flex;
    flex-direction: column;
    background-color: ${({ theme }) => theme.primaryDark};
    color: ${({ theme }) => theme.primaryLight};
    padding: 20px;
    border-radius: 50px;
    margin-bottom: 20px;
    a {
      text-decoration: none;
      background-color: ${({ theme }) => theme.primaryLight};
      color: ${({ theme }) => theme.primaryDark};
      padding: 5px;
      border-radius: 50px;
    }
  }
`;
