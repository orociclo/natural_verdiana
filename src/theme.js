export const theme = {
  primarySuperDark: "#659a63",
  primaryDark: "#92c03c",
  primaryDarkDark: "#006400",
  primaryDarkInverter: "#6d3fc3",
  primaryLight: "#EFFFFA",
  primaryLightPale: "#98fb98",
  primaryHover: "#04aa6d",
  mobile: "576px",
  primaryFontSize: "1rem",
  bigFontSize: "2rem",
  smallFontSize: "0.8rem",
}
