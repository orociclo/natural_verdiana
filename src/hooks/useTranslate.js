import { useState } from "react";

const useTranslate = ({ text = "", source_lang, target_lang }) => {
  const [response, setResponse] = useState(text);
  if (target_lang.toLowerCase() !== "es") {
    console.log("useTranslate....", text, target_lang);
    // create a new XMLHttpRequest
    const xhr = new XMLHttpRequest();
    const url = "https://api-free.deepl.com/v2/translate";
    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        const responseText = JSON.parse(xhr.responseText).translations[0].text;
        setResponse(responseText);
        const objectToRemember = { es: text };
        objectToRemember[target_lang] = responseText ? responseText : text;
        const objectToRememberString = JSON.stringify(objectToRemember);
        // TODO guardar cada traduccion por separado con el idioma: FRUTAS_BIO_en: ORGANIC FRUIT
        // const objectToRemembeKey = text.replace(/ /g, "_") + "_"+ target_lang;
        localStorage.setItem(
          text.replace(/ /g, "_"),
          JSON.stringify(objectToRememberString)
        );
        console.log(
          "New localstore ",
          text.replace(/ /g, "_"),
          JSON.stringify(objectToRememberString),
          objectToRemember
        );
      }
    };
    const data = `auth_key=6d5bd459-bb1c-3d6b-8e0f-3602afffffaa:fx&source_lang=${source_lang}&text=${text}&target_lang=${target_lang}`;
    xhr.send(data);
  }
  return { response };
};

export default useTranslate;
