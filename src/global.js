import { createGlobalStyle } from "styled-components";

import RobotoRegular from "./fonts/Roboto-Regular.ttf";

export const GlobalStyles = createGlobalStyle` @font-face {
  font-family: 'Roboto Regular';
  src: url(${RobotoRegular}) format('ttf')
}

*,
*::after,
*::before {
  box-sizing: border-box;
}

body {
  margin: 0px;
  padding: 0px;
  background: ${({ theme }) => theme.primaryLight};
  color:${({ theme }) => theme.primaryDark};
  text-rendering: optimizeLegibility;
  font-family: 'Roboto Regular',
  sans-serif;
  width: 100vw;
  user-select: none;
  overflow: hidden;
  outline: none;
}

#divMenu {
  z-index: 11;
  #divMenuContent {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
  }
}

#divContentContainer {
  padding-top: 45px;
  overflow-y: hidden;
  overflow-y: auto;
  height: 100vh;
}

#divLoader {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 90vh;
  width: 100%;
}

`;
